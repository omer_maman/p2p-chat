﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Security.Cryptography;

namespace Chat
{
    public partial class ChatForm : Form
    {
        private const int PORT = 8001;
        private string myIP = "", clientIp = "";
        Thread listen = null, clientsListener = null;
        string username;
        NetworkStream clientStream = null;

        public ChatForm(string username)
        {
            InitializeComponent();
            this.username = username;
            LVmsg.View = View.List;
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ipAdd in host.AddressList)
            {
                if (ipAdd.AddressFamily == AddressFamily.InterNetwork)
                {
                    myIP = ipAdd.ToString();
                    LBLip.Text += "\n" + myIP;
                    break;
                }
            }
            clientsListener = new Thread(ClientListener);
            clientsListener.Start();
        }

        private void BTNconnect_Click(object sender, EventArgs e)
        {
            if(listen == null)
            {

                if(IPValidator(TXTclientIP.Text))
                {
                    clientIp = TXTclientIP.Text;
                    TcpListener listener = new TcpListener(IPAddress.Parse(clientIp), PORT);
                    listener.Start();
                    TcpClient client = listener.AcceptTcpClient();
                    clientStream = client.GetStream();
                    LBLinvalid.Visible = false;
                    listen = new Thread(Listen);
                    listen.Start();
                    BTNconnect.Text = "Disconnect";
                    BTNsend.Enabled = true;
                }
                else
                {
                    LBLinvalid.Visible = true;
                }
            }
            else
            {
                listen.Abort();
                listen = null;
                BTNconnect.Text = "Connect";
                BTNsend.Enabled = false;
            }
        }

        private void ClientListener()
        {
            TcpListener listener = new TcpListener(IPAddress.Any, PORT);
            listener.Start();
            TcpClient client = listener.AcceptTcpClient();
            clientStream = client.GetStream(); 

            byte[] data = new byte[1024];
            int recv = clientStream.Read(data,0,1024);

            string msg = Encoding.ASCII.GetString(data, 0, recv);
            msg = Decrypt(msg);

            string[] clientDetails = msg.Split('@');
            clientIp = clientDetails[0];
            LVmsg.Invoke(new MethodInvoker(delegate
            {
                LVmsg.Items.Add(clientDetails[1] + " enter to chat");
            }));

            LBLinvalid.Visible = false;
            listen = new Thread(Listen);
            listen.Start();
            BTNconnect.Text = "Disconnect";
            BTNsend.Enabled = true;
        }
        private void Listen()
        {
            while(true)
            {
                string msg = "";
                byte[] data = new byte[1024];
                int recv = clientStream.Read(data,0,1024);

                msg = Encoding.ASCII.GetString(data, 0, recv);
                msg = Decrypt(msg);

                try
                {
                    string[] msgType = new string[2];
                    string sender = msg.Split('@')[0];
                    string context = msg.Substring(sender.Length + 1);
                    LVmsg.Invoke(new MethodInvoker(delegate
                    {
                        LVmsg.Items.Add(sender + ": " + context);
                    }));
                }
                catch(Exception e)
                {
                    
                }
            }
        }

        UnicodeEncoding ByteConverter = new UnicodeEncoding(); // to convert the string to byte[]
        RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(); // to make the key
        private string Encrypt(string msg)
        {
            byte[] encryptedData;
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                RSA.ImportParameters(RSA.ExportParameters(false));
                encryptedData = RSA.Encrypt(ByteConverter.GetBytes(msg), false);
            }
            msg = ByteConverter.GetString(encryptedData);
            return msg;
        }
        private string Decrypt(string msg)
        {
            byte[] decryptedData;
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                RSA.ImportParameters(RSA.ExportParameters(false));
                decryptedData = RSA.Decrypt(ByteConverter.GetBytes(msg), false);
            }
            msg = ByteConverter.GetString(decryptedData);
            return msg;
        }
        private bool IPValidator(string IP)
        {
            if (String.IsNullOrWhiteSpace(IP))
            {
                return false;
            }

            string[] splitValues = IP.Split('.');
            if (splitValues.Length != 4)
            {
                return false;
            }

            byte tempForParsing;

            return splitValues.All(r => byte.TryParse(r, out tempForParsing));
        }

        private void Send(string msg)
        {
            msg = username + "@" + msg;
            msg = Encrypt(msg);

            byte[] data = Encoding.ASCII.GetBytes(msg);
            clientStream.Write(data, 0 , data.Length);
        }

        private void BTNsend_Click(object sender, EventArgs e)
        {
            string text = TXTmsg.Text;
            TXTmsg.Text = "";
            Send(text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
