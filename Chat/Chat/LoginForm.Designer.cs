﻿namespace Chat
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LBLusername = new System.Windows.Forms.Label();
            this.TXTusername = new System.Windows.Forms.TextBox();
            this.BTNlogin = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LBLusername
            // 
            this.LBLusername.AutoSize = true;
            this.LBLusername.Location = new System.Drawing.Point(95, 121);
            this.LBLusername.Name = "LBLusername";
            this.LBLusername.Size = new System.Drawing.Size(84, 13);
            this.LBLusername.TabIndex = 0;
            this.LBLusername.Text = "Enter username:";
            // 
            // TXTusername
            // 
            this.TXTusername.Location = new System.Drawing.Point(185, 118);
            this.TXTusername.Name = "TXTusername";
            this.TXTusername.Size = new System.Drawing.Size(100, 20);
            this.TXTusername.TabIndex = 1;
            // 
            // BTNlogin
            // 
            this.BTNlogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.BTNlogin.Location = new System.Drawing.Point(140, 184);
            this.BTNlogin.Name = "BTNlogin";
            this.BTNlogin.Size = new System.Drawing.Size(108, 42);
            this.BTNlogin.TabIndex = 2;
            this.BTNlogin.Text = "Login";
            this.BTNlogin.UseVisualStyleBackColor = true;
            this.BTNlogin.Click += new System.EventHandler(this.BTNlogin_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(76, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Welcome to the chat";
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 320);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BTNlogin);
            this.Controls.Add(this.TXTusername);
            this.Controls.Add(this.LBLusername);
            this.Name = "LoginForm";
            this.Text = "Chat";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LBLusername;
        private System.Windows.Forms.TextBox TXTusername;
        private System.Windows.Forms.Button BTNlogin;
        private System.Windows.Forms.Label label1;
    }
}