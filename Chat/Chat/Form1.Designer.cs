﻿namespace Chat
{
    partial class ChatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTNconnect = new System.Windows.Forms.Button();
            this.LVmsg = new System.Windows.Forms.ListView();
            this.TXTmsg = new System.Windows.Forms.TextBox();
            this.BTNsend = new System.Windows.Forms.Button();
            this.LBLip = new System.Windows.Forms.Label();
            this.TXTclientIP = new System.Windows.Forms.TextBox();
            this.LBLclientIP = new System.Windows.Forms.Label();
            this.LBLinvalid = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BTNconnect
            // 
            this.BTNconnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.BTNconnect.Location = new System.Drawing.Point(686, 367);
            this.BTNconnect.Name = "BTNconnect";
            this.BTNconnect.Size = new System.Drawing.Size(119, 77);
            this.BTNconnect.TabIndex = 2;
            this.BTNconnect.Text = "Connect";
            this.BTNconnect.UseVisualStyleBackColor = true;
            this.BTNconnect.Click += new System.EventHandler(this.BTNconnect_Click);
            // 
            // LVmsg
            // 
            this.LVmsg.Location = new System.Drawing.Point(13, 13);
            this.LVmsg.Name = "LVmsg";
            this.LVmsg.Size = new System.Drawing.Size(615, 416);
            this.LVmsg.TabIndex = 3;
            this.LVmsg.UseCompatibleStateImageBehavior = false;
            // 
            // TXTmsg
            // 
            this.TXTmsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.TXTmsg.Location = new System.Drawing.Point(13, 436);
            this.TXTmsg.Name = "TXTmsg";
            this.TXTmsg.Size = new System.Drawing.Size(521, 23);
            this.TXTmsg.TabIndex = 4;
            // 
            // BTNsend
            // 
            this.BTNsend.Location = new System.Drawing.Point(541, 436);
            this.BTNsend.Name = "BTNsend";
            this.BTNsend.Size = new System.Drawing.Size(87, 23);
            this.BTNsend.TabIndex = 5;
            this.BTNsend.Text = "Send";
            this.BTNsend.UseVisualStyleBackColor = true;
            this.BTNsend.Click += new System.EventHandler(this.BTNsend_Click);
            // 
            // LBLip
            // 
            this.LBLip.AutoSize = true;
            this.LBLip.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.LBLip.Location = new System.Drawing.Point(660, 44);
            this.LBLip.Name = "LBLip";
            this.LBLip.Size = new System.Drawing.Size(173, 25);
            this.LBLip.TabIndex = 6;
            this.LBLip.Text = "Your IP address:";
            // 
            // TXTclientIP
            // 
            this.TXTclientIP.Location = new System.Drawing.Point(686, 310);
            this.TXTclientIP.Name = "TXTclientIP";
            this.TXTclientIP.Size = new System.Drawing.Size(119, 20);
            this.TXTclientIP.TabIndex = 7;
            // 
            // LBLclientIP
            // 
            this.LBLclientIP.AutoSize = true;
            this.LBLclientIP.Location = new System.Drawing.Point(686, 279);
            this.LBLclientIP.Name = "LBLclientIP";
            this.LBLclientIP.Size = new System.Drawing.Size(116, 13);
            this.LBLclientIP.TabIndex = 8;
            this.LBLclientIP.Text = "Enter client IP address:";
            // 
            // LBLinvalid
            // 
            this.LBLinvalid.AutoSize = true;
            this.LBLinvalid.Location = new System.Drawing.Point(700, 333);
            this.LBLinvalid.Name = "LBLinvalid";
            this.LBLinvalid.Size = new System.Drawing.Size(91, 13);
            this.LBLinvalid.TabIndex = 9;
            this.LBLinvalid.Text = "Invalid IP address";
            this.LBLinvalid.Visible = false;
            // 
            // ChatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 485);
            this.Controls.Add(this.LBLinvalid);
            this.Controls.Add(this.LBLclientIP);
            this.Controls.Add(this.TXTclientIP);
            this.Controls.Add(this.LBLip);
            this.Controls.Add(this.BTNsend);
            this.Controls.Add(this.TXTmsg);
            this.Controls.Add(this.LVmsg);
            this.Controls.Add(this.BTNconnect);
            this.Name = "ChatForm";
            this.Text = "Chat";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BTNconnect;
        private System.Windows.Forms.ListView LVmsg;
        private System.Windows.Forms.TextBox TXTmsg;
        private System.Windows.Forms.Button BTNsend;
        private System.Windows.Forms.Label LBLip;
        private System.Windows.Forms.TextBox TXTclientIP;
        private System.Windows.Forms.Label LBLclientIP;
        private System.Windows.Forms.Label LBLinvalid;
    }
}

